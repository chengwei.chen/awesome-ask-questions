# Awesome-Ask-Questions

如何問一個好問題是現代工作者都需要具備的基本技能

這個 GitLab Project 嘗試用來收集各種關於「如何良好的 Ask Questions」的學習資源

- [文章 - How To Ask Questions The Smart Way](http://www.catb.org/~esr/faqs/smart-questions.html)（[中文翻譯 - 提問的智慧](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way)）
- [GitHub Repo - 别像弱智一样提问 Stop-Ask-Questions-The-Stupid-Ways](https://github.com/tangx/Stop-Ask-Questions-The-Stupid-Ways)
- [文章 - 如何有效地報告錯誤 How to Report Bugs Effectively](https://www.chiark.greenend.org.uk/~sgtatham/bugs-tw.html)
- [部落格 Complete Think - 如何有效的回報問題 How to Report Problems Effectively](https://rickhw.github.io/2018/03/18/SQA/How-To-Report-A-Defect-or-Bug/)
- [書籍 - 《你會問問題嗎？問對問題比回答問題更重要！從正確發問、找出答案到形成策略，百位成功企業家教你如何精準提問，帶出學習型高成長團隊》](https://www.cite.com.tw/book?id=85780)
- [conventional: comments](https://conventionalcomments.org/)（內容在談，如何寫出有意義的 Comments。）

---

## 隨筆

- 為什麼會有這個 GitLab Project？因為 Rick 在 SRE Taiwan FB 社團發了一個[貼文](https://www.facebook.com/groups/sre.taiwan/posts/2707650186067535/)。

- 歡迎送 MR 來，如果有收到好的 MR，有空時會按下 Merge 啦～